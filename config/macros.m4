# AX_PROG_PAPI
# ------------
AC_DEFUN([AX_PROG_PAPI],
[

    AC_ARG_WITH(papi,
        AC_HELP_STRING(
            [--with-papi@<:@=DIR@:>@],
            [specify where to find PAPI libraries and includes]
        ),
        [papi_paths="${withval}"],
        [papi_paths="not_set"] dnl List of possible default paths
    )

    # AS_IF([test "x$with_readline" != xnot_set],
    #     [AC_MSG_NOTICE([Determining if papi is in folder $papi_paths])]

    if test "${papi_paths}" != "not_set" ; then
        PAPI_DIR=${papi_paths}
        echo "setting PAPI home via '--with-papi' option: $PAPI_DIR"
    else
        if test "${PAPI_HOME+set}" = set; then
            PAPI_DIR=${PAPI_HOME}
            echo "setting PAPI home via '\$PAPI_HOME' env. variable: $PAPI_DIR"
        else #last hope
            PAPI_AVAIL_PATH=`which papi_avail`
            if test "x${PAPI_AVAIL_PATH}" != "x" ; then
                PAPI_DIR=${papi_paths}
                PAPI_BIN_DIR=`dirname ${PAPI_AVAIL_PATH}`
                PAPI_DIR=`dirname ${PAPI_BIN_DIR}`
            echo "setting PAPI home via 'which papi_avail': $PAPI_DIR"
            else
                AC_MSG_ERROR([Error PAPI home directory was not found! Please specify with --with-papi=<path>])
            fi
        fi 
    fi

    AC_SUBST([PAPI_DIR])
])


# AX_PROG_EXTRAE
# ------------
AC_DEFUN([AX_PROG_EXTRAE],
[

    AC_ARG_WITH(extrae,
        AC_HELP_STRING(
            [--with-extrae@<:@=DIR@:>@],
            [specify where to find EXTRAE libraries and includes]
        ),
        [extrae_paths="${withval}"],
        [extrae_paths="not_set"] dnl List of possible default paths
    )

    # AS_IF([test "x$with_readline" != xnot_set],
    #     [AC_MSG_NOTICE([Determining if extrae is in folder $extrae_paths])]

    if test "${extrae_paths}" != "not_set" ; then
        EXTRAE_DIR=${extrae_paths}
        echo "setting EXTRAE home via '--with-extrae' option: $EXTRAE_DIR"
    else
        if test "${EXTRAE_HOME+set}" = set; then
            EXTRAE_DIR=${EXTRAE_HOME}
            echo "setting EXTRAE home via '\$EXTRAE_HOME' env. variable: $EXTRAE_DIR"
        else #last hope
            EXTRAE_AVAIL_PATH=`which extrae-cmd`
            if test "x${EXTRAE_AVAIL_PATH}" != "x" ; then
                EXTRAE_DIR=${extrae_paths}
                EXTRAE_BIN_DIR=`dirname ${EXTRAE_AVAIL_PATH}`
                EXTRAE_DIR=`dirname ${EXTRAE_BIN_DIR}`
            echo "setting EXTRAE home via 'which extrae-cmd': $EXTRAE_DIR"
            else
                AC_MSG_ERROR([Error EXTRAE home directory was not found! Please specify with --with-extrae=<path>])
            fi
        fi 
    fi

    AC_SUBST([EXTRAE_DIR])
])

# AX_PROG_CUDA
# ------------
AC_DEFUN([AX_PROG_CUDA],
[

    AC_ARG_WITH(cuda,
        AC_HELP_STRING(
            [--with-cuda@<:@=DIR@:>@],
            [specify where to find CUDA libraries and includes]
        ),
        [cuda_paths="${withval}"],
        [cuda_paths="not_set"] dnl List of possible default paths
    )

    # AS_IF([test "x$with_readline" != xnot_set],
    #     [AC_MSG_NOTICE([Determining if papi is in folder $papi_paths])]

    if test "${cuda_paths}" != "not_set" ; then
        CUDA_DIR=${cuda_paths}
        echo "setting CUDA home via '--with-cuda' option: $CUDA_DIR"
    else
        if test "${CUDA_HOME+set}" = set; then
            CUDA_DIR=${CUDA_HOME}
            echo "setting CUDA home via '\$CUDA_HOME' env. variable: $CUDA_DIR"
        else
            if test "${CUDA_HOME+set}" = set; then
                CUDA_DIR=${CUDA_HOME}
                echo "setting CUDA home via '\$CUDA_HOME' env. variable: $CUDA_DIR"
            else #last hope
                CUDA_AVAIL_PATH=`which nvcc`
                if test "x${CUDA_AVAIL_PATH}" != "x" ; then
                    CUDA_BIN_DIR=`dirname ${CUDA_AVAIL_PATH}`
                    CUDA_DIR=`dirname ${CUDA_BIN_DIR}`
                echo "setting CUDA home via 'which nvcc': $CUDA_DIR"
                else
                    AC_MSG_ERROR([Error CUDA home directory was not found! Please specify with --with-cuda=<path>])
                fi
            fi
        fi 
    fi
    AC_SUBST([CUDA_DIR])
])