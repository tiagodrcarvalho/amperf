#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <ramperf.h>

#define CORE_EVENTS  3
#define L2_EVENTS 2
#define SCF_EVENTS 2

const char* events[]= {
    "carmel:::CPU_CYCLES",
    "carmel:::L1D_CACHE",
    "carmel:::L1D_CACHE_REFILL",
};

const char* l2_events[]= {
    "carmel:::L2D_CACHE",
    "carmel:::L2D_CACHE_REFILL"
};

const char* scf_events[]= {
    "carmel:::L3D_CACHE",
    "carmel:::L3D_CACHE_REFILL",
};


#define ITERS 5
#define TASKS 16
int main(){
    printf("TIME: %lu\n",ramperf_get_time());
    // ramperf_events(events, CORE_EVENTS);
    ramperf_xavier_events(events, CORE_EVENTS, l2_events, L2_EVENTS, scf_events, SCF_EVENTS);
    //just a workaround to get the number of threads
    int num_threads;
    #pragma omp parallel
    #pragma omp single
        num_threads = omp_get_num_threads();
    ramperf_init(num_threads,TASKS);
    int vals[TASKS];
    for (int i = 0; i < TASKS;i++){
        vals[i] = 1;
    }
    for(int i = 0; i < ITERS; i++){
        printf("IT %d\n",i);
        ramperf_start_tdg();
        #pragma omp parallel
        #pragma omp single
        {
            for(int t = 0; t < TASKS; t++){
                #pragma omp task
                {
                    ramperf_task_start(t);
                    vals[t] += omp_get_thread_num();
                    //printf("(it %d) My task is %d and I'm running in thread %d\n",i,t,omp_get_thread_num());
                    ramperf_task_end(t);
                }
            }
        }
        measurement_t* results = ramperf_stop_tdg();
        for(int r = 0; r < ramperf_get_tdg_size();r++){
            measurement_t task = results[r];
            printf("Task %u, cpu %u, thread %d, start %lu, time %lu\n", 
                task.task_id,
                task.cpu_id,
                task.thread_id,
                task.start_time,
                task.time);
            printf("\t");
            for(int e = 0; e < CORE_EVENTS; e++){
                printf(" %s=%llu",events[e],task.perf_counter[e]);
            }
            printf("\n");
            printf("\t");
            for(int e = 0; e < L2_EVENTS; e++){
                printf(" %s=%llu",l2_events[e],task.perf_counter[CORE_EVENTS+e]);
            }
            printf("\n");
            printf("\t");
            for(int e = 0; e < SCF_EVENTS; e++){
                printf(" %s=%llu",scf_events[e],task.perf_counter[CORE_EVENTS+L2_EVENTS+e]);
            }
            printf("\n");
        }
        free(results);
    }
    ramperf_stop();
}