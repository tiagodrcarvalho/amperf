#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <omp.h>
#include <amperf.h>

#define CORE_EVENTS  3
#define L2_EVENTS 2
#define SCF_EVENTS 2

const char* events[]= {
    "carmel:::CPU_CYCLES",
    "carmel:::L1D_CACHE",
    "carmel:::L1D_CACHE_REFILL",
};

const char* l2_events[]= {
    "carmel:::L2D_CACHE",
    "carmel:::L2D_CACHE_REFILL"
};

const char* scf_events[]= {
    "carmel:::L3D_CACHE",
    "carmel:::L3D_CACHE_REFILL",
};


#define ITERS 5
#define TASKS 16
int main(){
    // amperf_events(events, CORE_EVENTS);
    amperf_xavier_events(events, CORE_EVENTS, l2_events, L2_EVENTS, scf_events, SCF_EVENTS);
    //just a workaround to get the number of threads
    int num_threads;
    #pragma omp parallel
    #pragma omp single
        num_threads = omp_get_num_threads();
    amperf_init(num_threads,TASKS,ITERS);
    int vals[TASKS];
    for (int i = 0; i < TASKS;i++){
        vals[i] = 1;
    }
    for(int i = 0; i < ITERS; i++){
        #pragma omp parallel
        #pragma omp single
        {
            for(int t = 0; t < TASKS; t++){
                #pragma omp task
                {
                    amperf_task_start(t);
                    vals[t] += omp_get_thread_num();
                    //printf("(it %d) My task is %d and I'm running in thread %d\n",i,t,omp_get_thread_num());
                    amperf_task_end(t);
                }
            }
        }
    }
    FILE *out = fopen("output.csv", "w");
    amperf_to_csv(out);
    amperf_stop();
}