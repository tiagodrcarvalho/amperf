#include "cudamperf.h"
#include <stdio.h>
#include <time.h>
#include <ctype.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>
#include <execinfo.h>
#include <extrae.h>

#include <cuda.h>
#include <cupti.h>

pthread_mutex_t gpu_mutex;
void
print_trace (void)
{
  int leng = 100;
  void *array[leng];
  char **strings;
  int size, i;

  size = backtrace (array, leng);
  strings = backtrace_symbols (array, size);
  if (strings != NULL)
  {

    printf ("Obtained %d stack frames.\n", size);
    for (i = 0; i < size; i++)
      printf ("%s\n", strings[i]);
  }

  free (strings);
}

#ifdef _WIN32
  #define FILE_SEPARATOR '\\'
#else
  #define FILE_SEPARATOR '/'
#endif

#define CHECK_CU_ERROR(err, cufunc)                                     \
  if (err != CUDA_SUCCESS)                                              \
    {                                                                   \
      printf ("%s:%d: error %d for CUDA Driver API function '%s'\n",    \
              __FILE__, __LINE__, err, cufunc);                         \
      exit(-1);                                                         \
    }

#define CHECK_CUPTI_ERROR(err, cuptifunc)                               \
  if (err != CUPTI_SUCCESS)                                             \
    {                                                                   \
      const char *errstr;                                               \
      cuptiGetResultString(err, &errstr);                               \
      printf ("%s:%d:Error %s for CUPTI API function '%s'.\n",          \
              __FILE__, __LINE__, errstr, cuptifunc);                   \
      exit(-1);                                                         \
    }

#define MAX_NUM_EVENTS 8
#define NUM_ALL_EVENTS MAX_NUM_EVENTS
const char *all_cuda_events[NUM_ALL_EVENTS] = {
    "active_cycles_pm",
    "active_warps_pm",
    "inst_executed",
    "inst_issued0", 
    "inst_issued1",
    "thread_inst_executed",
    "l2_subp1_read_tex_sector_queries",
    "l2_subp0_read_tex_sector_queries",
    // "not_predicated_off_thread_inst_executed",
    // "global_load",
    // "global_store",
};

int started = 0;

typedef struct cupti_eventData_st {
  CUpti_EventGroupSets *groupSets;
  CUpti_EventID *eventIds;
  uint32_t num_events;
} cupti_eventData;

// Structure to hold data collected by callback
typedef struct RuntimeApiTrace_st {
  cupti_eventData *eventData;
  uint64_t start_timestamp;
  const char* task_name;
  const char* omp_region_name;
} RuntimeApiTrace_t;


CUpti_SubscriberHandle subscriber;
RuntimeApiTrace_t trace;
CUdevice dev = 0;
CUcontext context = 0;
uint64_t timestamp_reference;
int is_first;


void cudamperf_enter_omp_region(const char* region_name){
    trace.omp_region_name = region_name;
}

void cudamperf_exit_omp_region(){
    trace.omp_region_name = NULL;
}

void cudamperf_enter_context(){
  if(started == 0){
    //just time execution
    cuptiGetTimestamp(&trace.start_timestamp);
    return;
  }
  pthread_mutex_lock(&gpu_mutex);
  CUresult err;
  err = cuCtxCreate(&context, 0, dev);
  CHECK_CU_ERROR(err, "cuCtxCreate");
  // printf("ENTER CONTEXT %d\n",context);

  cupti_eventData* cuptiEvent = trace.eventData;
  size_t eventIdArraySizeBytes  = sizeof(CUpti_EventID)* cuptiEvent->num_events;
  cuptiEventGroupSetsCreate(context, eventIdArraySizeBytes, cuptiEvent->eventIds, &cuptiEvent->groupSets);
  cuptiGetTimestamp(&trace.start_timestamp);
}


unsigned long cudamperf_get_timestamp(){
  uint64_t timestamp;
  cuptiGetTimestamp(&timestamp);
  return timestamp;
}

uint64_t cudamperf_exit_context(){

  uint64_t end_timestamp;
  cuptiGetTimestamp(&end_timestamp);
  end_timestamp -= trace.start_timestamp;
  if(started != 0){
    
    CUptiResult cuptiErr;
    CUpti_EventGroupSets *groupSets = trace.eventData->groupSets;
    cuptiErr = cuptiEventGroupSetsDestroy(groupSets);
    CHECK_CUPTI_ERROR(cuptiErr, "cuptiEventGroupSetsDestroy");
    
    cuCtxDestroy(context);
    // printf("LEAVED CONTEXT %d\n",context);
    context = NULL;
    pthread_mutex_unlock(&gpu_mutex);
  }
  
  Extrae_event(100111111,end_timestamp);
  return end_timestamp;
}

void cudamperf_enter_task(const char* name){
  cudamperf_enter_context();
  trace.task_name = name;
  // printf("Started task %s with context %d!\n",name, (uint32_t)context);
}

void cudamperf_exit_task(){
  // printf("Finished task %s!\n",trace.task_name);
  cudamperf_exit_context();
  trace.task_name = NULL;
}


void CUPTIAPI
getEventValueCallback(void *userdata, CUpti_CallbackDomain domain,
                      CUpti_CallbackId cbid, const CUpti_CallbackData *cbInfo)
{
  CUptiResult cuptiErr;
  RuntimeApiTrace_t *traceData = (RuntimeApiTrace_t*)userdata;
  size_t bytesRead; 
     
  // This callback is enabled only for launch so we shouldn't see anything else.
  if ((cbid != CUPTI_RUNTIME_TRACE_CBID_cudaLaunch_v3020) &&
      (cbid != CUPTI_RUNTIME_TRACE_CBID_cudaLaunchKernel_v7000))
  {
    printf("%s:%d: unexpected cbid %d\n", __FILE__, __LINE__, cbid);
    exit(1);
  }
  
  if (cbInfo->callbackSite == CUPTI_API_ENTER) {
    
    cudaDeviceSynchronize();
    // cuptiGetTimestamp(&traceData->start_timestamp);
    // traceData->start_timestamp -=timestamp_reference;

    
    // printf("CUPTI_API_ENTER");
    // printf("CONTEXT_ENTER: actual %u vs expected %u\n",cbInfo->context, context);
    // if (cbInfo->context != context){
    //   return;
    // }
    
    cuptiErr = cuptiSetEventCollectionMode(cbInfo->context, 
                                           CUPTI_EVENT_COLLECTION_MODE_KERNEL);
    CHECK_CUPTI_ERROR(cuptiErr, "cuptiSetEventCollectionMode");
    CUpti_EventGroupSets *sets = traceData->eventData->groupSets;
    for (int i = 0; i < sets->numSets; i++) {
      cuptiErr = cuptiEventGroupSetEnable(&trace.eventData->groupSets->sets[i]);
      CHECK_CUPTI_ERROR(cuptiErr, "cuptiEventGroupSetEnable");
    }
    
  }
    
  if (cbInfo->callbackSite == CUPTI_API_EXIT) {
    
    // printf("CUPTI_API_EXIT");
    // printf("CONTEXT_EXIT: actual %u vs expected %u\n",cbInfo->context, context);
    // if (cbInfo->context != context){
    //   return;
    // }

    // cuptiGetTimestamp(&end_timestamp);
    // end_timestamp -= timestamp_reference;
    CUpti_EventGroupSets *groupSets = traceData->eventData->groupSets;
    uint64_t end_timestamp = 0;
    uint32_t num_sets = groupSets->numSets;
    for (int i = 0; i < num_sets; i++) {
        size_t size;
        CUpti_EventGroupSet setI = groupSets->sets[i];
        uint32_t numEventGroups = setI.numEventGroups;

        // Iterate over each event group in the set
        for (int j = 0; j < numEventGroups; j++) {
          CUpti_EventGroup group = setI.eventGroups[j];
          // get events number in each group
          uint32_t num_events;
          uint32_t num_instances;
          size = sizeof(num_events);
          cuptiErr = cuptiEventGroupGetAttribute(group, CUPTI_EVENT_GROUP_ATTR_NUM_EVENTS, &size, &num_events);
          CHECK_CUPTI_ERROR(cuptiErr, "cuptiEventGroupGetAttribute");
          // get instances number in each group
          size = sizeof(num_instances);
          cuptiErr = cuptiEventGroupGetAttribute(group, CUPTI_EVENT_GROUP_ATTR_INSTANCE_COUNT, &size, &num_instances);
          CHECK_CUPTI_ERROR(cuptiErr, "cuptiEventGroupGetAttribute");

          // calculate the size of the array to hold all the event ids in the group
          size_t event_id_size = sizeof(CUpti_EventID) * num_events;
          // for each group, calculate the size of the array to hold all the counter values for all instances in the group
          size_t counter_values_size = sizeof(uint64_t) * num_events * num_instances;
          
          // allocate memory for the event ids and counter values buffers
          CUpti_EventID* event_ids_buffer = (CUpti_EventID *)malloc(event_id_size);
          if (event_ids_buffer == NULL) {
            printf("%s:%d: failed to allocate memory.\n", __FILE__, __LINE__);
            exit(1);
          }
          uint64_t * counters_buffer = (uint64_t *)malloc(counter_values_size);
          if (counters_buffer == NULL) {
            printf("%s:%d: failed to allocate memory.\n", __FILE__, __LINE__);
            exit(1);
          }
          
          size_t num_event_ids;
          // read events from group and automatically reset counters
          num_event_ids = 0;
          // wait for the execution to finish before getting values
          cudaDeviceSynchronize();
          // if (end_timestamp == 0){
          //   const char comma = is_first == 1? ' ':','; 
          //   is_first = 0; 
    
          //   fprintf(traceData->out_file,"%c\n {\n\t\"omp_region\": \"%s\",\n\t\"task\": \"%s\",\n\t\"execution_time\": %lu,\n\t\"performance_counters\": {\n", 
          //         comma,
          //         traceData->omp_region_name,
          //         traceData->task_name,
          //         // traceData->start_timestamp,
          //         // end_timestamp,
          //         end_timestamp - traceData->start_timestamp);
          // }
          
          cuptiErr = cuptiEventGroupReadAllEvents(
            group, CUPTI_EVENT_READ_FLAG_NONE,
            &counter_values_size, counters_buffer,
            &event_id_size, event_ids_buffer,
            &num_event_ids
          );
          CHECK_CUPTI_ERROR(cuptiErr, "cuptiEventGroupReadAllEvents");

          if (num_event_ids != num_events) {
            printf("%s:%d: number of read GPU events (%lu) does not match number of events in group (%d).\n", 
                __FILE__, __LINE__, num_event_ids, num_events);
            exit(1);
          }


          unsigned long long* event_values = (unsigned long long*)malloc(sizeof(unsigned long long)*num_event_ids);

          // printf("For group %d\n",j);
          for(int e = 0; e< num_event_ids;e++){
            uint64_t total = 0;
            if (num_instances > 0){
              for(int inst = 0; inst < num_instances;inst++){
                total += counters_buffer[inst*num_event_ids+e];
                // printf("\t instance %d of event %d with value %lu\n",inst,event_ids_buffer[e], counters_buffer[inst*num_event_ids+e]);

              }
              total /=num_instances;
            }
            event_values[e] = total;
            // printf("\tevent %d with value %llu\n",event_ids_buffer[e], event_values[e]);
          }
          Extrae_nevent(num_event_ids, event_ids_buffer, event_values);
          //   const char name[60] ;
          //   size = sizeof(name);
          //   cuptiErr = cuptiEventGetAttribute(event_ids_buffer[e],
          //                                   CUPTI_EVENT_ATTR_NAME,
          //                                   &size,
          //                                   (void *)name);
          //    fprintf(traceData->out_file,"\t \"%s\": [ ",
          //         cuptiErr != CUPTI_SUCCESS?"<unknown>":name);
          //         // num_instances);
          //   uint64_t total = 0;
          //   if (num_instances > 0){
          //     fprintf(traceData->out_file,"%lu",counters_buffer[e]);
          //     for(int inst = 1; inst < num_instances;inst++){
          //       fprintf(traceData->out_file,", %lu",counters_buffer[inst*num_event_ids+e]);
          //     }
          //   }
          //   const char comma = e+1 >= num_event_ids && i+1 >= num_sets && j+1 >= numEventGroups?' ':',';
          //   fprintf(traceData->out_file,"]%c\n",comma);
          // }

          free(event_ids_buffer);
          free(counters_buffer);
          free(event_values);
        }
    }
    // fprintf(traceData->out_file,"\n\t}\n }");
    
    for (int i = 0; i < groupSets->numSets; i++) {
        // Iterate over each event group in the set
        cuptiErr = cuptiEventGroupSetDisable(&groupSets->sets[i]);
        CHECK_CUPTI_ERROR(cuptiErr, "cuptiEventGroupSetDisable");
    }
    
  }
}

size_t trimwhitespace(char *out, size_t len, const char *str)
{
  if(len == 0)
    return 0;

  const char *end;
  size_t out_size;

  // Trim leading space
  while(isspace((unsigned char)*str)) str++;

  if(*str == 0)  // All spaces?
  {
    *out = 0;
    return 1;
  }

  // Trim trailing space
  end = str + strlen(str) - 1;
  int had_trailing_space = 0;
  while(end > str && isspace((unsigned char)*end)){
    end--;
    had_trailing_space = 1;
  }
    
  end++;

  // Set output size to minimum of trimmed string length and buffer size minus 1
  out_size = (end - str) < len-1 ? (end - str) : len-had_trailing_space;

  // Copy trimmed string and add null terminator
  memcpy(out, str, out_size);
  out[out_size] = 0;

  return out_size;
}


void add_event_from_name(CUdevice dev, const char* eventName, int event_position){
  CUptiResult cuptiErr;
  // printf("Adding %s in position %d\n",eventName,event_position);
  cuptiErr= cuptiEventGetIdFromName(dev, eventName, & trace.eventData->eventIds[event_position]);
  if (cuptiErr != CUPTI_SUCCESS) 
  { 
    printf("Invalid event name: '%s'. Will abort execution.\n", eventName);
    exit(1);
    // return -1; 
  }
  // printf("Added %d in position %d\n",trace.eventData->eventIds[event_position],event_position);

}

int set_event_ids(CUdevice dev){
 
  // cupti_eventData* cuptiEvent = trace.eventData;
  // cuptiEvent->eventIds
  //first try in environment variable
  char *config_file;
  // Make sure envar actually exists
  if(!(config_file=getenv(CUDAMPERF_CONFIG_ENV_VAR))){
      fprintf(stderr, "The environment variable %s was not found. Will just measure kernel execution time.\n", CUDAMPERF_CONFIG_ENV_VAR);
      // fprintf(stderr, "The environment variable %s was not found. Will use all the necessary performance counters.\n", CUDAMPERF_CONFIG_ENV_VAR);
      
      // cuptiEvent->num_events = NUM_ALL_EVENTS;
      // size_t size_of_eventIds = sizeof(CUpti_EventID)* cuptiEvent->num_events;
      // cuptiEvent->eventIds = (CUpti_EventID*) malloc(size_of_eventIds);
      // for(int i = 0; i < cuptiEvent->num_events; i++){
        
      //   add_event_from_name(dev, all_cuda_events[i], i);
      // }
      return 0;
  }
  CUptiResult cuptiErr;
  FILE *pmc_file = fopen(config_file, "r");
  char * line = NULL;
  size_t len = 0;
  ssize_t read;
  if (pmc_file == NULL){
      printf("Could not open file defined in %s: %s\n", CUDAMPERF_CONFIG_ENV_VAR, config_file);
      exit(EXIT_FAILURE);
  }
  int readV = getline(&line, &len, pmc_file);
  if (readV == -1) {
    printf("Could not read any line from pmc file %s\n", config_file);
    exit(EXIT_FAILURE);
  }
  
  trace.eventData = (cupti_eventData*)malloc(sizeof(cupti_eventData));
  cupti_eventData* cuptiEvent = trace.eventData;

  cuptiEvent->num_events = (uint32_t)atoi(line); //first line should have number of performance counters
  if(cuptiEvent->num_events > MAX_NUM_EVENTS){
    fprintf(stderr, "Configuration file specifies more than %d events, only the first %d will be considered.\n", cuptiEvent->num_events, MAX_NUM_EVENTS);
    cuptiEvent->num_events = MAX_NUM_EVENTS;
  }
  size_t size_of_eventIds = sizeof(CUpti_EventID)* cuptiEvent->num_events;
  cuptiEvent->eventIds = (CUpti_EventID*) malloc(size_of_eventIds);
  
  for(int i = 0; i < cuptiEvent->num_events;i++){
    int readV = getline(&line, &len, pmc_file);
    if (readV == -1) {
      printf("Expected a line for performance counter #%d, but no more lines (or no content) to read in pmc file %s, line #%d\n", i, config_file, i + 2);
      exit(EXIT_FAILURE);
    }
    char event_id_str[readV];
    trimwhitespace(event_id_str, readV, line);
    trace.eventData->eventIds[i] = (CUpti_EventID)atoi(event_id_str);
    // add_event_from_name(dev, event_name, i);
  }
  fclose(pmc_file);
  if (line)
      free(line);
  return cuptiEvent->num_events;
}  



void cudamperf_start(){
  CUresult err;
  CUptiResult cuptiErr;
  is_first = 1;
  
  // int computeCapabilityMajor=0;
  // int computeCapabilityMinor=0;
  int deviceNum = 0;
  int deviceCount;
  char deviceName[32];
  uint32_t profile_all = 1;
  // char * file_name = strrchr( file_name, FILE_SEPARATOR);
  
  
  printf("[CUDAMPERF] Providing results to extrae\n");

    
  err = cuInit(0);
  CHECK_CU_ERROR(err, "cuInit");

  err = cuDeviceGetCount(&deviceCount);
  CHECK_CU_ERROR(err, "cuDeviceGetCount");


  //  printf("CUDA Device Number: %d\n", deviceNum);

  err = cuDeviceGet(&dev, deviceNum);
  CHECK_CU_ERROR(err, "cuDeviceGet");

  err = cuDeviceGetName(deviceName, 32, dev);
  CHECK_CU_ERROR(err, "cuDeviceGetName");

  
  int num_events = set_event_ids(dev);
  if (num_events == 0) {
      started = 0;
      return;
  }
  cuptiErr = cuptiSubscribe(&subscriber, (CUpti_CallbackFunc)getEventValueCallback, &trace);
  CHECK_CUPTI_ERROR(cuptiErr, "cuptiSubscribe");

  // cuptiErr = cuptiEnableCallback(1, subscriber, CUPTI_CB_DOMAIN_RUNTIME_API, 
  //                                CUPTI_RUNTIME_TRACE_CBID_cudaLaunch_v3020);
  // CHECK_CUPTI_ERROR(cuptiErr, "cuptiEnableCallback");
  cuptiErr = cuptiEnableCallback(1, subscriber, CUPTI_CB_DOMAIN_RUNTIME_API, 
                                CUPTI_RUNTIME_TRACE_CBID_cudaLaunchKernel_v7000);
  CHECK_CUPTI_ERROR(cuptiErr, "cuptiEnableCallback");
  cuptiGetTimestamp(&timestamp_reference);
  started = 1;
}

void cudamperf_stop(){
  timestamp_reference = 0;
  is_first = 1;
  
  if (started != 0) {

    CUptiResult cuptiErr;
    cuptiErr = cuptiUnsubscribe(subscriber);
    CHECK_CUPTI_ERROR(cuptiErr, "cuptiUnsubscribe");
    // free(trace.eventData->eventIds);
    // free(trace.eventData);
    trace.eventData = NULL;
  }
  
  started = 0;
}