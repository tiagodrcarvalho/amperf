#define _GNU_SOURCE
#include "amperf_ll.h"
#include "ramperf.h"
#include <string.h>
#include <stdlib.h>
#include <sched.h>
#include <pthread.h>
#include <omp.h>
#include <papi.h>

//measurements
typedef struct {
    //total number of tasks of the TDG
    uint32_t total_num_tasks;
    //1 measures all tasks, 0 measures specific tasks (see 'is_on' field)
    uint8_t measure_all_tasks;
    //a timestamp reference that points out to the init of papi
    uint64_t time_ref;
    //each position refers to a task: 1 is on, 0 is off
    uint8_t* is_on; 
    //each position refers to a task: 1 is on, 0 is off
    uint32_t* measured_tasks; 
    uint32_t num_measured_tasks; 
    //a 2D array: <task_id,measurement>
    measurement_t* results; 
} measurements_t;


static measurements_t ramperf_measurements;

static uint8_t to_measure(int task_id){
    return ramperf_measurements.measure_all_tasks || ramperf_measurements.is_on[task_id];
}

static void new_results(){
    ramperf_measurements.results = (measurement_t*)malloc(sizeof(measurement_t) * ramperf_measurements.total_num_tasks);
    for(int i = 0; i < ramperf_measurements.total_num_tasks; i++){
        if(to_measure(i)) //despite creating the complete array, we will only allocate memory for the actual measured tasks
            ramperf_measurements.results[i].perf_counter = (long_long*)calloc(amperf_total_num_events(),sizeof(long long));
    }
}


static void init_measurements_array(int num_threads, int total_num_tasks){
    
    ramperf_measurements.total_num_tasks = total_num_tasks;
    amperf_init_papi(num_threads);
    ramperf_measurements.time_ref = amperf_get_time();
}


/*
 * Initializes papi and measures all tasks of the TDG
 */
void ramperf_init(int num_threads, int total_num_tasks){
    ramperf_measurements.measure_all_tasks = 1;
    ramperf_measurements.num_measured_tasks = total_num_tasks;
    ramperf_measurements.measured_tasks = (uint32_t*)calloc(ramperf_measurements.num_measured_tasks, sizeof(uint32_t));
    for(int i=0;i < ramperf_measurements.total_num_tasks;i++){
        ramperf_measurements.measured_tasks[i] = i;
    }
    //measurements.is_on is empty, it does not matter, will measure all
    init_measurements_array(num_threads, total_num_tasks);
}

/*
 * Initializes papi and measures only one specific task of the TDG
 */
void ramperf_init_t(int num_threads, int total_num_tasks, int target_task){
    ramperf_measurements.measure_all_tasks = 0;
    ramperf_measurements.is_on = (uint8_t*)calloc(total_num_tasks, sizeof(uint8_t));
    ramperf_measurements.is_on[target_task] = 1;
    ramperf_measurements.num_measured_tasks = 1;
    ramperf_measurements.measured_tasks = (uint32_t*)calloc(ramperf_measurements.num_measured_tasks, sizeof(uint32_t));
    ramperf_measurements.measured_tasks[0] = target_task;
    init_measurements_array(num_threads, total_num_tasks);
}


/*
 * Initializes papi and measures a list of tasks of the TDG
 */
void ramperf_init_r(int num_threads, int total_num_tasks, int* target_tasks, int num_target_tasks){
    ramperf_measurements.measure_all_tasks = 0;
    ramperf_measurements.is_on = (uint8_t*)calloc(total_num_tasks, sizeof(uint8_t));
    ramperf_measurements.num_measured_tasks = num_target_tasks;
    ramperf_measurements.measured_tasks = (uint32_t*)calloc(ramperf_measurements.num_measured_tasks, sizeof(uint32_t));
    for(int i = 0; i < num_target_tasks; i++){
        ramperf_measurements.is_on[target_tasks[i]] = 1;
        ramperf_measurements.measured_tasks[i] = target_tasks[i];
    }
    init_measurements_array(num_threads, total_num_tasks);
}


static void free_measurements(){
    if(!ramperf_measurements.measure_all_tasks)
        free(ramperf_measurements.is_on);
    
    free(ramperf_measurements.measured_tasks);

    // free(ramperf_measurements.results);
}

void ramperf_stop(){
    amperf_fini_papi();
    free_measurements();
}



void ramperf_task_start(int task_id){
    if(!to_measure(task_id)){
        return;
    }
    
    measurement_t* initial = &ramperf_measurements.results[task_id];
    initial->task_id = task_id;
    initial->thread_id = omp_get_thread_num();
    initial->cpu_id = sched_getcpu();
    initial->start_time = amperf_get_time();
    amperf_read_papi(initial->perf_counter,initial->thread_id,initial->cpu_id);

}

void ramperf_task_end(int task_id){

    if(!to_measure(task_id)){
        return;
    }

    measurement_t* final = &ramperf_measurements.results[task_id];
    final->time = amperf_get_time() - final->start_time;
    final->start_time -=  ramperf_measurements.time_ref;
    long long final_value[amperf_total_num_events()];
    amperf_read_papi(final_value,final->thread_id,final->cpu_id);
    
    for(int i = 0; i < amperf_total_num_events();i++){
        final->perf_counter[i] = final_value[i] - final->perf_counter[i];
    }
}


void ramperf_start_tdg(){
    new_results();
}

/*
 * Do not forget to free the output!
 */
measurement_t* ramperf_stop_tdg(){
    return ramperf_measurements.results;
}

uint32_t* ramperf_get_measured_tasks(){
    return ramperf_measurements.measured_tasks;
}
uint32_t ramperf_get_tdg_size(){
    return ramperf_measurements.total_num_tasks;
}
uint32_t ramperf_get_total_measured_tasks(){
    return ramperf_measurements.num_measured_tasks;
}

