#define _GNU_SOURCE
#include "amperf_ll.h"
#include "amperf.h"
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include <sched.h>
#include <pthread.h>
#include <omp.h>
#include <papi.h>

typedef struct 
{
	uint32_t task_id;
	uint32_t thread_id;
	uint32_t cpu_id;
	uint64_t time;
    uint64_t start_time;
    long long* perf_counter;
} task_measurement_t;

//measurements
typedef struct {
    //total number of tasks of the TDG
    uint32_t total_num_tasks;
    //1 measures all tasks, 0 measures specific tasks (see 'is_on' field)
    uint8_t measure_all_tasks;
    //a timestamp reference that points out to the init of papi
    uint64_t time_ref;
    //each position refers to a task: 1 is on, 0 is off
    uint8_t* is_on; 
    //each position refers to the current number of measurements of that task
    uint32_t* num_measurements; 
    //a 2D array: <task_id,measurement>
    task_measurement_t** results; 
} task_measurements_t;


static task_measurements_t amperf_measurements;


static void init_measurements_array(int num_threads, int total_num_tasks, int max_num_results){
    
    amperf_measurements.total_num_tasks = total_num_tasks;
    amperf_measurements.num_measurements = (uint32_t*)calloc(total_num_tasks, sizeof(uint32_t));
    amperf_measurements.results = (task_measurement_t**)malloc(sizeof(task_measurement_t*) * total_num_tasks);
    for(int i = 0; i < total_num_tasks; i++){
        amperf_measurements.results[i] = (task_measurement_t*)malloc(sizeof(task_measurement_t) * max_num_results);
        for(int j = 0; j < max_num_results; j++){
            amperf_measurements.results[i][j].perf_counter = (long_long*)calloc(amperf_total_num_events(),sizeof(long long));
        }
    }
    
    amperf_init_papi(num_threads);
    amperf_measurements.time_ref = amperf_get_time();
}


/*
 * Initializes papi and measures all tasks of the TDG
 */
void amperf_init(int num_threads, int total_num_tasks, int max_num_results){
    amperf_measurements.measure_all_tasks = 1;
    //measurements.is_on is empty, it does not matter, will measure all
    init_measurements_array(num_threads, total_num_tasks,max_num_results);
}

/*
 * Initializes papi and measures only one specific task of the TDG
 */
void amperf_init_t(int num_threads, int total_num_tasks, int max_num_results, int target_task){
    amperf_measurements.measure_all_tasks = 0;
    amperf_measurements.is_on = (uint8_t*)calloc(total_num_tasks, sizeof(uint8_t));
    amperf_measurements.is_on[target_task] = 1;
    init_measurements_array(num_threads, total_num_tasks,max_num_results);
}


/*
 * Initializes papi and measures a list of tasks of the TDG
 */
void amperf_init_r(int num_threads, int total_num_tasks, int max_num_results, int* target_tasks, int num_target_tasks){
    amperf_measurements.measure_all_tasks = 0;
    amperf_measurements.is_on = (uint8_t*)calloc(total_num_tasks, sizeof(uint8_t));
    for(int i = 0; i < num_target_tasks; i++)
        amperf_measurements.is_on[target_tasks[i]] = 1;
    init_measurements_array(num_threads, total_num_tasks, max_num_results);
}


static void free_measurements(){
    if(!amperf_measurements.measure_all_tasks)
        free(amperf_measurements.is_on);
    free(amperf_measurements.num_measurements);
    for(int i = 0; i < amperf_measurements.total_num_tasks; i++){
        free(amperf_measurements.results[i]->perf_counter);
        free(amperf_measurements.results[i]);
    }
    free(amperf_measurements.results);
}

void amperf_stop(){
    amperf_fini_papi();
    free_measurements();
}

static uint8_t to_measure(int task_id){
    return amperf_measurements.measure_all_tasks || amperf_measurements.is_on[task_id];
}

void amperf_task_start(int task_id){
    if(!to_measure(task_id)){
        return;
    }
    
    task_measurement_t* initial = &amperf_measurements.results[task_id][amperf_measurements.num_measurements[task_id]];
    initial->task_id = task_id;
    initial->thread_id = omp_get_thread_num();
    initial->cpu_id = sched_getcpu();
    initial->start_time = amperf_get_time();
    amperf_read_papi(initial->perf_counter,initial->thread_id,initial->cpu_id);

}

void amperf_task_end(int task_id){

    if(!to_measure(task_id)){
        return;
    }

    task_measurement_t* final = &amperf_measurements.results[task_id][amperf_measurements.num_measurements[task_id]];
    final->time = amperf_get_time() - final->start_time;
    long long final_value[amperf_total_num_events()];
    amperf_read_papi(final_value,final->thread_id,final->cpu_id);
    
    
    for(int i = 0; i < amperf_total_num_events();i++){
        final->perf_counter[i] = final_value[i] - final->perf_counter[i];
    }
    amperf_measurements.num_measurements[task_id]++; //increase total num measurements for this task
}

void amperf_to_csv(FILE* output){
    fprintf(output,"task,thread,cpu,start(us),time(us)");
    for(int r = 0; r < amperf_get_num_core_events();r++){
        fprintf(output,",%s",amperf_get_core_event(r));
    }
    for(int r = 0; r < amperf_get_num_l2_events();r++){
        fprintf(output,",%s",amperf_get_l2_event(r));
    }
    for(int r = 0; r < amperf_get_num_scf_events();r++){
        fprintf(output,",%s",amperf_get_scf_event(r));
    }
    fprintf(output,"\n");
    for(int task_id = 0; task_id< amperf_measurements.total_num_tasks; task_id++){
        if(!to_measure(task_id)){
            continue;
        }
         uint32_t num_measurements = amperf_measurements.num_measurements[task_id];
        for(int r=0; r < num_measurements; r++){
            task_measurement_t *result = &amperf_measurements.results[task_id][r];
            fprintf(output,"%d,%d,%d,%lu,%lu",
                result->task_id,
                result->thread_id,
                result->cpu_id,
                result->start_time - amperf_measurements.time_ref,
                result->time
            );
            for(int e = 0; e < amperf_total_num_events();e++){
                fprintf(output,",%lld", result->perf_counter[e]);
            }
            fprintf(output,"\n");
        }
    }
}