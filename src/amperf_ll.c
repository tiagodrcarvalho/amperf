#define _GNU_SOURCE
#include "amperf_ll.h"
#include <string.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <sched.h>
#include <pthread.h>
#include <omp.h>
#include <papi.h>

#define NUM_CLUSTERS 4
#define CLUSTER_ATTRIBUTE ":cluster="
#define CLUSTER_ATTRIBUTE_SIZE 10 /*includes rhs size*/
#define MAX_NUM_RESULTS_PER_TASK 500

typedef struct {
    const char** events;
    int num_events;
} event_setup_t;


static event_setup_t amperf_core_events;
static event_setup_t amperf_l2_events;
static event_setup_t amperf_scf_events;
static int* amperf_eventsets;


int amperf_get_num_core_events(){
    return amperf_core_events.num_events;
}

int amperf_get_num_l2_events(){
    return amperf_l2_events.num_events;
}

int amperf_get_num_scf_events(){
    return amperf_scf_events.num_events;
}

const char* amperf_get_core_event(int pos){
    return amperf_core_events.events[pos];
}

const char* amperf_get_l2_event(int pos){
    return amperf_l2_events.events[pos];
}

const char* amperf_get_scf_event(int pos){
    return amperf_scf_events.events[pos];
}

u_int64_t get_time(){
  struct timeval tv;
  gettimeofday(&tv, 0);
  // u_int64_t t = tv.tv_sec*1e6+tv.tv_usec;
  return tv.tv_sec*1e6 + tv.tv_usec;
}

u_int64_t amperf_get_time(){
    return get_time();
}

void papi_check(const char *stage, int retval, int expected)
{
    if (retval != expected)
    {
        printf("[PAPI ERROR] %s (code %d): %s\n", stage, retval, PAPI_strerror(retval));
        exit(1);
    }
}

void papi_check_thread(int thread, const char *stage, int retval, int expected)
{
    if (retval != expected)
    {
        printf("[PAPI ERROR, THREAD %d] %s (code %d): %s\n", thread, stage, retval, PAPI_strerror(retval));
        exit(1);
    }
}

int amperf_total_num_events(){
    return amperf_core_events.num_events + amperf_l2_events.num_events + amperf_scf_events.num_events;
}

int total_events_in_main_thread(){
    return amperf_core_events.num_events + 
        amperf_l2_events.num_events*NUM_CLUSTERS + 
        amperf_scf_events.num_events;
}




void setup_event(event_setup_t* setup, const char** events, int num_events){
    
    setup->num_events = num_events;
    if(num_events == 0){
        return;
    }

    setup->events = malloc(sizeof(const char*) * num_events);
    for(int i = 0; i< num_events;i++){
        setup->events[i] = events[i];
    }
}

/*
 * Setup amperf by adding the PAPI core events to be used
 */
void amperf_events(const char** events, int num_events){
    setup_event(&amperf_core_events, events, num_events);
}

/*
 * Setup amperf by adding uncore PAPI events to be used. When reading the results, the PMCs are ordered by these levels: core, l2 and scf.
 * For instance, if 3 core, 2 l2 and 2 scf, the positions in the array of results would be: [core,core,core,l2,l2,scf,scf];
 * NOTE: this method is specifically for NVIDIA Jetson AGX Xavier platform, and deals with the uncore in a specific manner.
 */
void amperf_xavier_events(const char** core, int num_core_events, const char** l2, int num_l2_events, const char** scf, int num_scf_events){
    amperf_events(core,num_core_events);
    setup_event(&amperf_l2_events, l2, num_l2_events);
    setup_event(&amperf_scf_events, scf, num_scf_events);
}

void print_events(const char* type, event_setup_t* setup){
    if(setup->num_events == 0){
        // printf("\n");
        return;
    }
    printf("%d %s events",setup->num_events,type);
    printf(": %s",setup->events[0]);
    for(int i = 1; i< setup->num_events;i++){
        printf(", %s",setup->events[i]);
    }
    printf("\n");
}
void amperf_print_events(){
    print_events("core", &amperf_core_events);
    print_events("l2", &amperf_l2_events);
    print_events("scf", &amperf_scf_events);
}


void free_events(){
    free(amperf_core_events.events);
    free(amperf_l2_events.events);
    free(amperf_scf_events.events);
}


void amperf_init_papi(int num_threads){
    int papi_ret;
    papi_ret = PAPI_library_init(PAPI_VER_CURRENT);
    papi_check("library init",papi_ret,PAPI_VER_CURRENT);
    papi_ret = PAPI_thread_init(pthread_self);//omp_get_thread_num);
    papi_check("thread init",papi_ret,PAPI_OK);
    amperf_eventsets = malloc(sizeof(int)*num_threads);

    #pragma omp parallel
    //#pragma omp single
    {
    // for(int i  = 0; i < threads; i++){
        int i = omp_get_thread_num();
        amperf_eventsets[i] = PAPI_NULL;
        
        // int omp_thread_num = omp_get_thread_num();
        int papi_ret= PAPI_register_thread();
        papi_check_thread(i, "register",papi_ret,PAPI_OK);
        // int cpu = sched_getcpu();
        // unsigned long  tid = PAPI_thread_id();
        if ((PAPI_thread_id()) == (unsigned long int)-1){
            papi_check_thread(i,"thread id",1,PAPI_OK);
        }

        papi_ret=PAPI_create_eventset(&amperf_eventsets[i]);
        papi_check("eventset",papi_ret,PAPI_OK);
        
        for(int e= 0; e< amperf_core_events.num_events; e++){
            papi_ret = PAPI_add_named_event(amperf_eventsets[i], amperf_core_events.events[e]);
            papi_check("add core event",papi_ret,PAPI_OK);
        }
        if(i == 0){
            for(int c=0; c < NUM_CLUSTERS; c++)
                for(int e= 0; e< amperf_l2_events.num_events; e++){
                    char eventName[strlen(amperf_l2_events.events[e])+CLUSTER_ATTRIBUTE_SIZE];
                    sprintf(eventName,"%s"CLUSTER_ATTRIBUTE"%d",amperf_l2_events.events[e],c);
                    papi_ret = PAPI_add_named_event(amperf_eventsets[i], eventName);
                    papi_check("add L2 event",papi_ret,PAPI_OK);

                }

            for(int e= 0; e< amperf_scf_events.num_events; e++){
                papi_ret = PAPI_add_named_event(amperf_eventsets[i], amperf_scf_events.events[e]);
                papi_check("add SCF event",papi_ret,PAPI_OK);
            }
        }
        //PAPI_reset(eventset);
        papi_ret= PAPI_start(amperf_eventsets[i]);
        papi_check_thread(i,"start",papi_ret,PAPI_OK);
    }
}

void amperf_fini_papi(){
    #pragma omp parallel
    {
        int omp_thread_num = omp_get_thread_num();   
        long long count[total_events_in_main_thread()];
        PAPI_stop(amperf_eventsets[omp_thread_num],count);
        // papi_check_thread(omp_thread_num,"stop",papi_ret,PAPI_OK);
        PAPI_cleanup_eventset(amperf_eventsets[omp_thread_num]);
        PAPI_destroy_eventset(&amperf_eventsets[omp_thread_num]);
        int papi_ret=PAPI_unregister_thread();
        papi_check_thread(omp_thread_num,"unregister",papi_ret,PAPI_OK);
    }
    free_events();
}

void amperf_read_papi(long long* results,int omp_thread_num, int cpu){

    long long main_values[total_events_in_main_thread()];
    int papi_ret = PAPI_read(amperf_eventsets[0],main_values); //to get the uncore values
    papi_check("read main",papi_ret,PAPI_OK);

    int cluster = cpu >> 1;
    if(omp_thread_num == 0){

        for(int e = 0; e < amperf_core_events.num_events; e++){
            int main_pos = e;
            int this_pos = e;
            results[this_pos] = main_values[main_pos];
        }
    }else{
        papi_ret = PAPI_read(amperf_eventsets[omp_thread_num],results);
        papi_check_thread(omp_thread_num,"read",papi_ret,PAPI_OK);
    }

    //copy main values to this eventset
    for(int e = 0; e < amperf_l2_events.num_events; e++){
        int main_pos = amperf_core_events.num_events + cluster*amperf_l2_events.num_events + e;
        int this_pos = e+amperf_core_events.num_events;
        results[this_pos] = main_values[main_pos];
    }
    for(int e = 0; e < amperf_scf_events.num_events; e++){
        int main_pos = amperf_core_events.num_events + amperf_l2_events.num_events*NUM_CLUSTERS+e;
        int this_pos = e+amperf_core_events.num_events+amperf_l2_events.num_events;
        results[this_pos] = main_values[main_pos];
    }
}