# AMPERF
This is a high level API of PAPI to provide tracing mechanisms at the OpenMP "Task-level". 

The main idea for this tool was to:

1. Abstract PAPI: only requiring an array of named events
2. Reduce code instrumentation, currently, it only requires:
   *  1 or 2 LOC for setup 
   *  start and stop the measurement process
   *  start and stop around tasks
3. Automatically output results: currently with a CSV format

## Requirements
* OpenMP
* PAPI: https://icl.utk.edu/papi/
* autotools: `sudo apt install autoconf automake`

## Installing the library



To install the library in your system you just need to follow three steps: configure, make and install:

```
$ ./configure --prefix=<amperf_dir> --with-papi=<papi_dir>
$ make
$ make install 
```
<b>NOTE:</b> prepend sudo if <amperf_dir> requires root permissions (e.g. /opt/amperf)

Where:
* `<amperf_dir>` is the installation folder for amperf
* `<papi_dir>` is the folder where PAPI is installed

Note that if you do not specify the `--with-papi` option, the configuration will try to find PAPI directory via:
1. the $PAPI_HOME environment variable
2. by searching `papi_avail` tool via the `whereis` command

If the configuration is still not able to find it, it will emmit an error requesting you to specify the `--with-papi` option.

The compilation and installation will create the following structure:

```
amperf_dir
  |\_include
  |   \_amperf.h
   \_lib
      \_libamperf.a
      \_libamperf.so
      \_...
```



## Using amperf: code instrumentation

The instrumentation process is divided in four steps.

### 1. Setup events
First, we need to list, in the target applicaiton, the events we intend to use:
```c
#include <amperf.h>
const char* events[]= {
    "carmel:::CPU_CYCLES",
    "carmel:::L1D_CACHE",
    "carmel:::L1D_CACHE_REFILL",
};

const char* l2_events[]= {
    "carmel:::L2D_CACHE",
    "carmel:::L2D_CACHE_REFILL"
};

const char* scf_events[]= {
    "carmel:::L3D_CACHE",
    "carmel:::L3D_CACHE_REFILL",
};
#define CORE_EVENTS 3
#define L2_EVENTS 2
#define SCF_EVENTS 2
```
Then, we just need to send them to amperf:

```c
amperf_events(events, CORE_EVENTS);
amperf_uncore_events(l2_events, L2_EVENTS,scf_events, SCF_EVENTS);
```

<b>NOTE:</b> The setup for `amperf_uncore_events` is just for the use of Xavier's uncore events. If you are using a different system, or just want the core events, you don't need to use this method.

### 2. Initialize and stop amperf
To initialize amperf we need to indicate the number of OpenMP threads that will be used, the total number of tasks of the TDG, and the maximum number of results that might occur for each task. The last one can be just a high reference value, but never a lower value than the one needed. Since we might not want to measure all the tasks, amperf provides three ways to be initialized (use just one!):
1. Measure all tasks: the default behavior 
```c
amperf_init(num_threads, total_tasks_in_tdg, max_num_results);
```
2. Measure just a specific task: by id
```c
amperf_init_t(num_threads, total_tasks_in_tdg, max_num_results, target_task_id);
```
3. Measure a list of tasks: an array of tasks to measure
```c
amperf_init_l(num_threads, total_tasks_in_tdg, max_num_results, target_tasks_array);
```

To finish the measurements just invoke:
```c
amperf_stop();
```
### 3. Encapsulate tasks to measure
To provide measurements at the task level, each task must be encapsulated between `amperf_task_start(task_id)` and `amperf_task_end(task_id)` calls.

By using this you can manually control which tasks are monitored. On the other hand, you can just instrument all the tasks and control which ones are monitored by means of the previously selected `amperf_init` function (see bullet 1).

### 4. Print results in CSV format
After the execution of the monitored part, you can write the results in a CSV format, and you can control where to write it, by using the `amperf_to_csv` function:

```c
FILE *out = fopen("output.csv", "w");
amperf_to_csv(out); //or stdout to print to the console
amperf_stop();
```
<b>NOTE:</b> The results must be printed before `amperf_stop`, as the latter will free the memory allocated to the measurements.

## Compilation with amperf

To compile the application it is necessary to add amperf to the dependencies. You will need to add the following to your dependencies in the makefile:

```make
AMPERF_DIR = /path/to/amperf
AMPERF_CONF = -I$(AMPERF_DIR)/include -L$(AMPERF_DIR)/lib
```

Where `AMPERF_DIR` should map to the amperf directory, so it can find both header and library. Then, your compilation process needs to add the `$AMPERE_CONF` variable and `-lamperf`, similar to:

```make 
mycode: mycode.c
	gcc $(AMPERF_CONF) mycode.c -o mycode -lamperf
```
To run the application you will need to add this library to the LD_LIBRARY_PATH environment variable, such as:

```bash
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/path/to/amperf/lib
```

This will avoid problems in your application, specifially to avoid the error: `error while loading shared libraries: libamperf.so: cannot open shared object file: No such file or directory`.