#ifndef AMPERF_H
#define AMPERF_H 
#include <stdio.h>
#include <amperf_ll.h>

/*
 * Initializes amperf and measures all tasks of the TDG
 * NOTE: amperf_events should be invoked before amperf_init
 */
void amperf_init(
    int num_threads, int total_num_tasks, int max_num_results);

/*
 * Initializes amperf and measures only one specific task of the TDG
 */
void amperf_init_t(int num_threads, int total_num_tasks, int max_num_results, int target_task);


/*
 * Initializes amperf and measures a list of tasks of the TDG
 */
void amperf_init_l(int num_threads, int total_num_tasks, int max_num_results, int* target_tasks, int num_target_tasks);

void amperf_stop();

void amperf_task_start(int task_id);

void amperf_task_end(int task_id);

void amperf_to_csv(FILE* output);

#endif //AMPERF_H