#ifndef RAMPERF_H
#define RAMPERF_H 
#include <stdio.h>
#include <inttypes.h>
#include <amperf_ll.h>

typedef struct 
{
	uint32_t task_id;
	uint32_t thread_id;
	uint32_t cpu_id;
	uint64_t time;
    uint64_t start_time;
    long long* perf_counter;
} measurement_t;

uint32_t* ramperf_get_measured_tasks();
uint32_t ramperf_get_tdg_size();
uint32_t ramperf_get_total_measured_tasks();


/*
 * Initializes amperf and measures all tasks of the TDG
 * NOTE: amperf_events should be invoked before amperf_init
 */
void ramperf_init(int num_threads, int total_num_tasks);

/*
 * Initializes amperf and measures only one specific task of the TDG
 */
void ramperf_init_t(int num_threads, int total_num_tasks, int target_task);


/*
 * Initializes amperf and measures a list of tasks of the TDG
 */
void ramperf_init_l(int num_threads, int total_num_tasks, int* target_tasks, int num_target_tasks);

void ramperf_stop();

void ramperf_start_tdg();

/*
 * Do not forget to free the output!
 */
measurement_t* ramperf_stop_tdg();

void ramperf_task_start(int task_id);

void ramperf_task_end(int task_id);

///////////// Renaming low-level functions to maintain the "ramperf_" prefix


unsigned long (*ramperf_get_time)() = amperf_get_time;

int (*ramperf_total_num_events)() = amperf_total_num_events;

int (*ramperf_get_num_core_events)() = amperf_get_num_core_events;

int (*ramperf_get_num_l2_events)() = amperf_get_num_l2_events;

int (*ramperf_get_num_scf_events)() = amperf_get_num_scf_events;

const char* (*ramperf_get_core_event)(int pos) = amperf_get_core_event;

const char* (*ramperf_get_l2_event)(int pos) = amperf_get_l2_event;

const char* (*ramperf_get_scf_event)(int pos) = amperf_get_scf_event;

void (*ramperf_events)(const char** events, int num_events) = amperf_events;

void (*ramperf_xavier_events)(const char** core, int num_core_events, const char** l2, int num_l2_events, const char** scf, int num_scf_events) = amperf_xavier_events;




#endif //AMPERF_H