#ifndef AMPERFLL_H
#define AMPERFLL_H 
unsigned long amperf_get_time();

int amperf_total_num_events();

int amperf_get_num_core_events();

int amperf_get_num_l2_events();

int amperf_get_num_scf_events();

const char* amperf_get_core_event(int pos);

const char* amperf_get_l2_event(int pos);

const char* amperf_get_scf_event(int pos);

/*
 * Setup amperf by adding the PAPI core events to be used
 */
void amperf_events(const char** events, int num_events);

/*
 * Setup amperf by adding core and uncore PAPI events to be used
 * NOTE: this method is currently only working specifically for Xavier
 */
void amperf_xavier_events(const char** core, int num_core_events, const char** l2, int num_l2_events, const char** scf, int num_scf_events);


void amperf_print_events();


void amperf_init_papi(int num_threads);

void amperf_fini_papi();

void amperf_read_papi(long long* results,int omp_thread_num, int cpu);

#endif