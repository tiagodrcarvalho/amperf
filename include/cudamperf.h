#ifndef CUDAMPERF_H
#define CUDAMPERF_H

#ifdef __cplusplus
extern "C" {
#endif

#define CUDAMPERF_CONFIG_ENV_VAR "CUDAMPERF_CONFIG_FILE"


void cudamperf_start();

void cudamperf_stop();

void cudamperf_enter_context();

unsigned long cudamperf_exit_context();


unsigned long cudamperf_get_timestamp();

void cudamperf_enter_omp_region(const char* region_name);

void cudamperf_exit_omp_region();

void cudamperf_enter_task(const char* name);

void cudamperf_exit_task();
#ifdef __cplusplus
}
#endif


#endif //CUDAMPERF_H